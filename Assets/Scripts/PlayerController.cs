﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update

    public Vector3 targetPosition { get; set; }
    public Transform playerTransform { get; private set; }
    public bool isJourneyCompleted { get; set; }


    public float moveSpeed = 5;
    public float turnSpeed = 1f;
    public float stopDistance = 1f;
    public ParticleSystem VFX;

    private void Start()
    {
        playerTransform = transform;
    }


    private void UpdateRotation()
    {
        Vector3 direction = targetPosition - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(direction);
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * turnSpeed);
    }

    private void UpdatePosition()
    {
        transform.position += transform.forward * turnSpeed * Time.deltaTime;
    }


    //=============PUBLIC methods=============//
    public void UpdateMovement()
    {
        if (isJourneyCompleted)
            return;

        UpdateRotation();
        UpdatePosition();
    }

    //========================================//



    //=============COLLISION detection=========//
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Contains("Small"))
        {
            UIManager.Instance.UpdateScore();
        }

        if (other.gameObject.name.Contains("Destination"))
        {
            UIManager.Instance.ReachedAtDestination();
            isJourneyCompleted = true;
            if (!VFX.isPlaying)
            {
                VFX.Play();
            }
            Debug.Log("reached at destination");
        }
    }

}
