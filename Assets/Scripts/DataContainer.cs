﻿using UnityEngine;

[CreateAssetMenu(fileName = "DataContainer", menuName = "SetRandomWayPoints")]
public class DataContainer : ScriptableObject
{
    public SetRandomMovementPoints randomPoints;
}

[System.Serializable]
public class SetRandomMovementPoints
{

    [Header("Allocate random x y z position")]
    public Vector3[] wayPoints;

}