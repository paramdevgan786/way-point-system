﻿using UnityEngine;

public class RandomPointSpawner : MonoBehaviour
{

    public GameObject objectToSpawn;
    public int spawnCount; // How many objects should be spawned
    public float MinX = 0;
    public float MaxX = 200;
    public float MinZ = 0;
    public float MaxZ = 200;

    private void Start()
    {
        SpawnObject();
    }

    private void SpawnObject()
    {

        for (int i = 0; i < spawnCount; i++)
        {

            float x = Random.Range(MinX, MaxX);
            float y = (objectToSpawn.transform.localScale.y / 2);
            float z = Random.Range(MinZ, MaxZ);

            Instantiate(objectToSpawn, new Vector3(x, y, z), Quaternion.identity);
        }
    }

}
