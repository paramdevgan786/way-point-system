﻿using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIManager : MonoBehaviour
{
    #region public
    public static UIManager Instance { get; private set; }
    #endregion


    [SerializeField]
    private Text smallBoxCollisionCount;
    [SerializeField]
    private Transform destinationTitle;
    [SerializeField]
    private Transform gameOverPanel;
    //temp variable to store the count of collisons with small boxes 
    private int collisonCount = 0;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

    }

    public void UpdateScore()
    {
        collisonCount += 1;
        smallBoxCollisionCount.text = collisonCount + "";
    }

    public void ReachedAtDestination()
    {
        destinationTitle.gameObject.SetActive(true);
        StartCoroutine(enableReset());
    }

    private IEnumerator enableReset()
    {
        yield return new WaitForSeconds(2f);
        gameOverPanel.gameObject.SetActive(true);
    }

    public void Reset()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
