﻿using UnityEngine;

public class WayPointController : MonoBehaviour
{
    #region public vars//can make serializable fields too
    public DataContainer dataContainer;//designer's scriptable
    #endregion

    #region  Private references
    private float pendingDistance = 0;
    private int currentPoint = 0;
    private float stopDistance = 0.1f;
    private PlayerController playerController;
    #endregion


    void Start()
    {
        playerController = FindObjectOfType<PlayerController>();
        playerController.targetPosition = dataContainer.randomPoints.wayPoints[currentPoint];
        CreateTargets();
    }

    void CreateTargets()
    {
        int count = dataContainer.randomPoints.wayPoints.Length;
        for (int index = 0; index < count; index++)
        {
            GameObject targetObj = Instantiate(Resources.Load("Target", typeof(GameObject)), dataContainer.randomPoints.wayPoints[index], Quaternion.identity) as GameObject;
            if (index == count - 1)
            {
                targetObj.name = "Destination";//using this code just to detect the collsion with the last object from the waypoint
            }
        }
    }


    void Update()
    {
        pendingDistance = Vector3.Distance(playerController.playerTransform.position, dataContainer.randomPoints.wayPoints[currentPoint]);

        if (pendingDistance > stopDistance)
        {
            playerController.UpdateMovement();
        }
        else
        {
            if (currentPoint + 1 == dataContainer.randomPoints.wayPoints.Length)
            {
                currentPoint = 0;//reset back
            }
            else
            {
                currentPoint += 1;//increment to new point
            }
            playerController.targetPosition = dataContainer.randomPoints.wayPoints[currentPoint];
        }

    }

}
